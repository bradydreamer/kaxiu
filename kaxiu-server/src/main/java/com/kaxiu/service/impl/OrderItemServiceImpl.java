package com.kaxiu.service.impl;

import com.kaxiu.persistent.entity.OrderItem;
import com.kaxiu.persistent.mapper.OrderItemMapper;
import com.kaxiu.service.IOrderItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 订单项 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class OrderItemServiceImpl extends ServiceImpl<OrderItemMapper, OrderItem> implements IOrderItemService {

}
