package com.kaxiu.service;

import com.kaxiu.persistent.entity.ServicePersonal;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 维修人员信息表 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface IServicePersonalService extends IService<ServicePersonal> {

}
