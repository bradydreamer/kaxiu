package com.kaxiu.config.shiro;


import com.kaxiu.config.shiro.jwt.JwtConfig;
import com.kaxiu.config.shiro.jwt.JwtToken;
import com.kaxiu.persistent.entity.BasicUser;
import com.kaxiu.persistent.entity.SysPremission;
import com.kaxiu.persistent.entity.SysRole;
import com.kaxiu.service.IBasicUserService;
import com.kaxiu.service.ISysPremissionService;
import com.kaxiu.service.ISysRoleService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.subject.PrincipalCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by EalenXie on 2018/11/26 12:12.
 * Realm 的一个配置管理类 allRealm()方法得到所有的realm
 */
@Component
public class ShiroRealmConfig {

    @Resource
    private JwtConfig jwtConfig;

    private static final Logger logger = LoggerFactory.getLogger(ShiroRealmConfig.class);

    @Autowired
    private IBasicUserService userService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPremissionService premissionService;

    @Autowired
    private UserRealm realm;

    /**
     * 配置所有自定义的realm,方便起见,应对可能有多个realm的情况
     */
    public List<Realm> allRealm() {
        List<Realm> realmList = new LinkedList<>();
        AuthorizingRealm jwtRealm = jwtRealm();
        realmList.add(jwtRealm);
//        realmList.add(realm);
        return Collections.unmodifiableList(realmList);
    }


    /**
     * 自定义 JWT的 Realm
     * 重写 Realm 的 supports() 方法是通过 JWT 进行登录判断的关键
     */
    private AuthorizingRealm jwtRealm() {
        AuthorizingRealm jwtRealm = new AuthorizingRealm() {
            /**
             * 注意坑点 : 必须重写此方法，不然Shiro会报错
             * 因为创建了 JWTToken 用于替换Shiro原生 token,所以必须在此方法中显式的进行替换，否则在进行判断时会一直失败
             */
            @Override
            public boolean supports(AuthenticationToken token) {
                return token instanceof JwtToken;
            }

            /**
             * 授权
             * @param principals
             * @return
             */
            @Override
            protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
                JwtToken token = (JwtToken) principals.getPrimaryPrincipal();
                String wxOpenId = jwtConfig.getWxOpenIdByToken(token.getToken());
                SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
                //从数据库中获取当前登录用户的详细信息
                BasicUser userInfo = userService.findByOpenId(wxOpenId);
                if (null != userInfo) {
                    List<SysRole> roles = roleService.getByUserId(userInfo.getId());
                    for(SysRole userrole:roles){
                        Long rolid = userrole.getId();//角色id
                        authorizationInfo.addRole(userrole.getName());//添加角色名字
                        List<SysPremission> premissions = premissionService.getByRoleId(rolid);
                        for(SysPremission p:premissions){
                            //System.out.println("角色下面的权限:"+gson.toJson(p));
                            if(com.baomidou.mybatisplus.core.toolkit.StringUtils.isNotEmpty(p.getPerms())){
                                authorizationInfo.addStringPermission(p.getPerms());
                            }
                        }
                    }
                } else {
                    throw new AuthorizationException();
                }
                logger.info("###【微信-获取角色成功】[SessionId] => {}", SecurityUtils.getSubject().getSession().getId());
                return authorizationInfo;
//                return new SimpleAuthorizationInfo();
            }

            /**
             * 校验 验证token逻辑
             */
            @Override
            protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) {
                String jwtToken = (String) token.getCredentials();
                String wxOpenId = jwtConfig.getWxOpenIdByToken(jwtToken);
                String sessionKey = jwtConfig.getSessionKeyByToken(jwtToken);
                if (StringUtils.isEmpty(wxOpenId))
                    throw new AuthenticationException("user account not exits , please check your token");
                if (StringUtils.isEmpty(sessionKey))
                    throw new AuthenticationException("sessionKey is invalid , please check your token");
                if (!jwtConfig.verifyToken(jwtToken))
                    throw new AuthenticationException("token is invalid , please check your token");
                return new SimpleAuthenticationInfo(token, token, getName());
            }
        };
        jwtRealm.setCredentialsMatcher(credentialsMatcher());
        return jwtRealm;
    }

    /**
     * 注意坑点 : 密码校验 , 这里因为是JWT形式,就无需密码校验和加密,直接让其返回为true(如果不设置的话,该值默认为false,即始终验证不通过)
     */
    private CredentialsMatcher credentialsMatcher() {
        return (token, info) -> true;
    }
}
