package com.kaxiu.common.base;

import com.alibaba.fastjson.JSONObject;
import com.kaxiu.persistent.entity.BasicUser;
import com.kaxiu.vo.ResultDataMsg;
import com.kaxiu.vo.ResultDataWrap;
import com.kaxiu.vo.enums.AttestationEnum;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthenticatedException;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * 抽象
 *
 * @author : Hui.Wang [huzi.wh@gmail.com]
 * @version : 1.0
 * @created on  : 2017/2/8  下午9:06
 */
public abstract class AbstractBaseController {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    private String viewPrefix;

    public AbstractBaseController() {
    }

    public AbstractBaseController(String viewPrefix) {
        this.viewPrefix = viewPrefix;
    }

    public ResultDataWrap buildResult() {
        return new ResultDataWrap(ResultDataMsg.DEFAULT_SUCCESS);
    }

    public ResultDataWrap buildResult(Object o) {
        return new ResultDataWrap(o);
    }

    public ResultDataWrap buildFailResult(String msg) {
        return new ResultDataWrap(msg, 500);
    }

    public ResultDataWrap buildFailResult() {
        return new ResultDataWrap(ResultDataMsg.DEFAULT_ERROR);
    }

    /**
     * 校验 用户状态
     * @param user
     * @return
     */
    public Boolean validServer(BasicUser user){
        if(user.getStatus() == 1 && user.getAuditStatus() == AttestationEnum.CERTIFIED
        && !user.getDeleted()){
            return true;
        }
        return false;
    }

}
