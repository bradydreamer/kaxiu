package com.kaxiu.persistent.entity;

import java.math.BigDecimal;
import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 订单项
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class OrderItem extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 订单id
     */
    private Long orderId;

    /**
     * 维修类别(商品)id
     */
    private Long productId;

    /**
     * 价格
     */
    private BigDecimal amount;

    /**
     * 备注
     */
    private String remark;

}
