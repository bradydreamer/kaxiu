package com.kaxiu.persistent.entity;

import com.kaxiu.common.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 角色权限中间表
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@Accessors(chain = true)
public class SysPermissionRole {

private static final long serialVersionUID=1L;

    /**
     * id
     */
    private Long id;

    /**
     * 角色id
     */
    private String roleId;

    /**
     * 权限id
     */
    private String permissionId;


}
