package com.kaxiu.persistent.entity;

import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;

import com.kaxiu.vo.enums.AttestationEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户信息
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class BasicUser extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 昵称
     */
    private String realName;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 性别:0未知;1男;2女
     */
    private Integer gender;

    /**
     * 微信 openid
     */
    private String openId;

    /**
     * 微信 unionid
     */
    private String unionId;

    /**
     * 微信 APP ID
     */
    private String appId;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 语言
     */
    private String language;

    /**
     * city
     */
    private String city;

    /**
     * province
     */
    private String province;

    /**
     * country
     */
    private String country;

    /**
     * 登陆密码，管理端用户使用
     */
    private String password;

    /**
     * 用户认证状态，1：已认证，0：未认证，3：认证中
     */
    private AttestationEnum auditStatus;


}
