package com.kaxiu.persistent.entity;

import java.math.BigDecimal;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.TableField;
import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 维修单详情
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class RepairOrder extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 用户 openid,下单用户
     */
    private Long orderId;

    /**
     * 用户 openid,下单用户
     */
    private Long userId;

    /**
     * 当前维修人员 id
     */
    private Long maintenanceId;

    /**
     * 维修类别(商品)id
     */
    private Long serviceId;

    /**
     * 总费用价格
     */
    private BigDecimal amount;

    /**
     * 现场照片,微信图片，最多九张
     */
    private String scenePhoto;

    /**
     * 维修员到场拍的照片
     */
    private String beforeRepairPhoto;

    /**
     * 维修结束拍的照片
     */
    private String afterRepairPhoto;

    /**
     * 备注
     */
    private String remark;


    /**
     * 联系人
     */
    private String contact;

    /**
     * 联系人手机号
     */
    private String mobile;

    /**
     * 评价
     */
    private String review;

    /**
     * 评分, 0-5
     */
    private Integer starLevel;

    /**
     * 订单位置：经纬度、速度、精确度、高度、垂直精度、水平精度.JSON
     * {"address":"北京市东城区正义路2号","latitude":39.90469,"errMsg":"chooseLocation:ok","name":"北京市经济委员会西北","longitude":116.40717}
     */
    @TableField(el = "location, typeHandler=com.kaxiu.config.mybatis.JSONObjectTypeHandler")
    private JSONObject location;

//    @TableField(el = "location, typeHandler=com.kaxiu.config.mybatis.JSONObjectTypeHandler")
    private String personalLocation;

    /**
     * 服务结束时间
     */
    private LocalDateTime finishedTime;

}
