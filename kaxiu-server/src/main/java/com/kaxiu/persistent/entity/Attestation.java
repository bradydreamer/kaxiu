package com.kaxiu.persistent.entity;

import com.kaxiu.common.base.BaseEntity;

import com.kaxiu.vo.enums.AttestationEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 认证记录
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class Attestation extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 用户 open ID
     */
    private Long userId;

    /**
     * 认证类型：维修员：1、商户：2、代理：3
     */
    private Integer auditType;

    /**
     * 真实姓名，如果是商户就是商户名称
     */
    private String realName;

    /**
     * 认证信息
     */
    private String auditInfo;

    /**
     * 用户认证状态，1：已审核，0：审核失败，3：审核中
     */
    private AttestationEnum auditStatus;

}
