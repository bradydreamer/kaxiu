package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.SendOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 维修-派单记录 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface SendOrderMapper extends BaseMapper<SendOrder> {

}
