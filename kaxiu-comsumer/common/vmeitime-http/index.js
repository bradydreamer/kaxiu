import http from './interface'

var appid = 'wx5dc5c3f431a47092'

http.interceptor.response = (res) => {
	if(res.data && res.data.status == 401){
		// token过期或未授权
		console.log('token过期或未授权,需要重新进行授权')
		uni.login({
			success(loginRes) {
				http.request({
				    url: '/wx/'+ appid + '/user/login',
				    data: {code: loginRes.code}
				}).then(function(loginValue) {
					uni.setStorageSync('token', loginValue.data.data)
					// 重新获取token成功，跳转首页
					uni.reLaunch({
						url: '../../pages/main/main'
					})
				});
			}
		})
	}
}

//设置请求前拦截器
http.interceptor.request = (config) => {
	config.header = {
		Authorization: uni.getStorageSync('token')
	}
}

export const baseUrl = http.config.baseUrl

// 小程序登陆
export const getSessionKey = (data) => {
	
    return http.request({
        url: '/wx/'+ appid + '/user/login',
        data,
    })
}

// 初始化字典数据
export const getDictionary = () => {

    return http.request({
        url: '/kaxiu/sys-dic',
        method: 'POST', 
        data: ['tip', 'amapKey'],
		// handle:true
    })
}

// 初始化服务类别
export const getCategories = () => {
    return http.request({
        url: '/kaxiu/service-category',
        method: 'GET', 
    })
}

// 初始化服务类别
export const getUserInfo = (data) => {
    return http.request({
        url: '/wx/'+ appid + '/user/info',
        method: 'GET', 
		data,
    })
}

// 获取验证码
export const getVerfication = (data) => {
    return http.request({
        url: '/kaxiu/code',
        method: 'GET', 
		data,
    })
}

// 绑定用户手机号
export const bindUser = (data) => {
    return http.request({
        url: '/kaxiu/basic-user/bind-user',
        method: 'GET', 
		data,
    })
}

// 下单
export const takeOrders = (data) => {
    return http.request({
        url: '/kaxiu/orders',
        method: 'POST', 
		data,
    })
}

// 订单列表
export const getOrderList = (data) => {
    return http.request({
        url: '/kaxiu/orders/list',
        method: 'GET', 
		data,
    })
}

// 根据id获取订单详情
export const getOrderInfo = (data) => {
    return http.request({
        url: '/kaxiu/orders/info',
        method: 'GET', 
		data,
    })
}

// 根据id点评
export const rate = (data) => {
    return http.request({
        url: '/kaxiu/orders/rate',
        method: 'GET', 
		data,
    })
}

// 默认全部导出  import api from '@/common/vmeitime-http/'
export default {
	baseUrl,
	getSessionKey,
    getDictionary,
	getCategories,
	getUserInfo,
	getVerfication,
	bindUser,
	takeOrders,
	getOrderList,
	getOrderInfo,
	rate
}